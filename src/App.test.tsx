import React from 'react';
import renderer from 'react-test-renderer';
import App from './App';

test('render App', () => {
  const component = renderer.create(<App />);

  expect(component.toJSON()).toEqual({ type: 'div', props: {}, children: ['hi'] });
});
