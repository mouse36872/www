import React from 'react';
import PropTypes from 'prop-types';
import './App.css';

interface Props {
  hello?: boolean;
}

const App: React.FC<Props> = ({
  hello,
}) => {
  let text = '';

  if (hello) {
    text = 'hello';
  } else {
    text = 'hi';
  }

  return (<div>{text}</div>);
};

App.propTypes = {
  hello: PropTypes.bool,
};

App.defaultProps = {
  hello: false,
};

export default App;
